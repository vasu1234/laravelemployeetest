<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $fillable = [
       
            'first_name',
            'last_name',
            'middle_name',
            'address',
            'zip',
            'department_id',
            'city_id',
            'state_id',
            'country_id',
            'birthdate',
            'date_hired',
            
    ];


    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
