<?php



if (!function_exists('has_permission')) {
    function has_permission($permissionName)
    {
        $session = Session::get('permission');
        if (in_array($permissionName, $session)) {
            return true;
        } else {
            return false;
        }
    }
}