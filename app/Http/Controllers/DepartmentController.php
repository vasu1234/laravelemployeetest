<?php

namespace App\Http\Controllers;
use App\Models\Department;
use Validator;
use DB;
use Exception;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('department/department', ['title' => 'Department']);
    }

    public function departmentList(Request $request)
    {
        if ($request->ajax()) {
            return datatables()->of(Department::select("id", "name")->get())

                ->make(true);
        }
    }
}
