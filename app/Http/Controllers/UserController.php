<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Models\User;
use Validator;
use DB;
use Exception;
use Illuminate\Support\Facades\Hash;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user/user', ['title' => "User"]);
    }





    public function userList(Request $request)
    {
        if ($request->ajax()) {
            return datatables()->of(User::select("id", "name", "last_name", "email" )->get())

                ->make(true);
        }
    }

    public function userEdit(Request $requst, $id)
    {
        $user = User::findorfail($id);

        return view('user/edit', [ 'user' => $user ]);
    }

 



}