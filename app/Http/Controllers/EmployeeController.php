<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Country;
use App\Models\City;
use App\Models\State;
use App\Models\Department;

use Validator;
use DB;
use Exception;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $county = Country::get();
        $city = City::get();
        $state = State::get();
        $department = Department::get();
        return view('employee/employee', ['title' => 'Employee','country'=>$county,'state'=>$state,'city'=>$city,'department'=>$department]);
    }

    public function employeeList(Request $request)
    {
        if ($request->ajax()) {
            return datatables()->of(Employee::select("id", DB::raw("concat(first_name,' ',middle_name,' ',last_name)as name"),"address", "zip","date_hired","deleted_at")->get())

                ->make(true);
        }
    }
}
