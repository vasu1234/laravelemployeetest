<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Department;
use App\Models\Employee;

class CommonController extends Controller
{
    public function getCountries(){
        
        $countries = Country::with('states.cities')->get();
        return json_encode(["status"=>true,"countries" =>$countries]);
    }

    //department apis
    public function createDepartment(Request $request){
        $department = Department::create($request->all());
        return json_encode(["status"=>true,"department"=>$department]);
    } 
    
    public function getDepartments(Request $request){
        $departments =  Department::get();
        return json_encode($departments);
    }
    public function searchDepartments(Request $request){
        $departments =  Department::where('name',$request->name."%")->get();
        return json_encode($departments);
    }
    public function updateDepartment(Request $request){
        if($request->has('id')){
            Department::where('id',$request->id)->update($request->data);

            return json_encode(["status"=>true]);
        }
        return json_encode(["status"=>false]);
    }
    public function deleteDepartment(Request $request){
        if($request->has('id')){
            Department::where('id',$request->id)->delete();

            return json_encode(["status"=>true]);
        }
        return json_encode(["status"=>false]);
    }

    //employee apis 

    public function createEmployee(Request $request){
        $employee = Employee::create($request->all());
        return json_encode(["status" => true,"employee"=>$employee]);
    } 
    
    public function getEmployees(Request $request){
        $employees = Employee::get();
        return json_encode($employees);
    }
    public function searchEmployees(Request $request){
        $value = $request->value;
        $employees = Employee::where('name',$value."%")->get();
        if(!$employees->count()){
            $employees = Employee::whereHas('department',function($q)use($value){$q->where("name",$value."%");})->get();
        }
        return json_encode($employees);
    }
    public function updateEmployee(Request $request){
        if($request->has('id')){
            Employee::where('id',$request->id)->update($request->data);

            return json_encode(["status"=>true]);
        }
        return json_encode(["status"=>false]);
    }
    public function deleteEmployee(Request $request){
        if($request->has('id')){
            Employee::where('id',$request->id)->delete();

            return json_encode(["status"=>true]);
        }
        return json_encode(["status"=>false]);
    }



}
