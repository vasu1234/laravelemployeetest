'user strict';

function successalert(msg){
    return `<div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> ${msg}
            </div>`;
}
function erroralert(msg){
    return `<div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> ${msg}
            </div>`;
}

function spanerror(id, msg){
    $("#"+id).after(`<span style="color:red !important;" class="span-error">${msg}</span>`);
    $('html, body').animate({
        scrollTop: $("#"+id).offset().top-100
    }, 400);
}
function scrollto(id){
    $('html, body').animate({
        scrollTop: $(id).offset().top-100
    }, 400);
}