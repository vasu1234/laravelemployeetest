$(document).ready(function () {
    if(home){
        if ($(window).width() >= 768) {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 200) {
                    $(".header-bottom").addClass('sticky');
                    $(".container-megamenu.vertical").removeClass('open');
                } else {
                    $(".header-bottom").removeClass('sticky');
                    $(".container-megamenu.vertical").addClass('open');
                }
            });
        }
        $(".container-megamenu.vertical").addClass('open');    
        $(".megamenuToogle-wrapper").click(function (e) { 
            e.preventDefault();
            e.stopPropagation();
        });
    }else{
        if ($(window).width() >= 768) {  
            $(window).scroll(function(){
                if($(this).scrollTop() > 200){
                    $(".header-bottom").addClass('sticky');
                    $(".container-megamenu.vertical").removeClass('open');
                } else{
                    $(".header-bottom").removeClass('sticky');
                }
            });
        }
    }
});

var home;