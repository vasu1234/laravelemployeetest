$(document).ready(function () {
    $("#add_news").click(function (e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        $('.error').remove();
        formData = new FormData($('#news_form')[0]);
//        $('#add_news').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        /* return false; */
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/news/add/' + id;
        } else {
            var url = base_url + 'admin/news/add/';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add career 
    $("#add_career").click(function (e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        $('.error').remove();
        formData = new FormData($('#career_form')[0]);
//        $('#add_news').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        /* return false; */
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/career/add/' + id;
        } else {
            var url = base_url + 'admin/career/add/';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add Job 
    $("#add_more_job").click(function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/career/addMoreJob',
            cache: false,
            success: function (data) {
                if (data) {
                    $(".more_job").append(data);

                }
            }
        });
    });

    // add contact
    $("#add_contact").click(function (e) {
        e.preventDefault();
        $('.error').remove();
        formData = new FormData($('#contact_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/contact/add/' + id;
        } else {
            var url = base_url + 'admin/contact/add/';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add footer contact
    $("#add_footer_contact").click(function (e) {
        e.preventDefault();
        $('.error').remove();
        formData = new FormData($('#footer_contact_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/footer/contact_add/' + id;
        } else {
            var url = base_url + 'admin/footer/contact_add';
        }
        console.log(formData);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add footer links
    $("#add_footer_links").click(function (e) {
        e.preventDefault();
        $('.error').remove();
        formData = new FormData($('#footer_link_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/footer/link_add/' + id;
        } else {
            var url = base_url + 'admin/footer/link_add';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    window.location = base_url + 'admin/footer/links_edit/' + id;
//                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add footer about
    $("#add_footer_about").click(function (e) {
        e.preventDefault();
        $('.error').remove();
        formData = new FormData($('#footer_about_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/footer/about_add/' + id;
        } else {
            var url = base_url + 'admin/footer/about_add';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //add footer about
    $("#add_footer_newsletter").click(function (e) {
        e.preventDefault();
        $('.error').remove();
        formData = new FormData($('#footer_newsletter_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = base_url + 'admin/footer/newsletter_add/' + id;
        } else {
            var url = base_url + 'admin/footer/newsletter_add';
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $('#position').change(function () {
        var position = $(this).val();
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/footer/get_position_data/' + position,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    console.log(data.data);
                    $('#id').val(data.data.id);
                    $('#link_heading').val(data.data.link_heading);
                    $('#link_title_five').val(data.data.link_title_five);
                    $('#link_title_four').val(data.data.link_title_four);
                    $('#link_title_one').val(data.data.link_title_one);
                    $('#link_title_three').val(data.data.link_title_three);
                    $('#link_title_two').val(data.data.link_title_two);
                    $('#link_url_five').val(data.data.link_url_five);
                    $('#link_url_four').val(data.data.link_url_four);
                    $('#link_url_one').val(data.data.link_url_one);
                    $('#link_url_three').val(data.data.link_url_three);
                    $('#link_url_two').val(data.data.link_url_two);
                } else {
                    $('#id').val('');
                    $('#link_heading').val('');
                    $('#link_title_five').val('');
                    $('#link_title_four').val('');
                    $('#link_title_one').val('');
                    $('#link_title_three').val('');
                    $('#link_title_two').val('');
                    $('#link_url_five').val('');
                    $('#link_url_four').val('');
                    $('#link_url_one').val('');
                    $('#link_url_three').val('');
                    $('#link_url_two').val('');
                }
            }
        });
    });


    $('#subscribeform').submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        $('#email-error').remove();
        formData = new FormData($('#subscribeform')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: base_url + "news/subscribe",
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    $('#success').val(data.msg);
                    setTimeout(function () {
                        $('#email').val('');
                        $('#success').fadeOut('fast');
                    }, 3000);
                } else {
                    $('#' + data.key).after("<sapn class=\"error help-block\"  style=\"color:#ECB100\">" + data.msg + "</sapn>");
                    setTimeout(function () {
                        $('#email').val('');
                        $('.error').fadeOut('fast');
                    }, 3000);
                }
            }
        });
    });

});

function deleteJob(id) {
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/career/delete_confirmation/' + id,
        cache: false,
        success: function (data) {
            if (data) {
                $('.modal_container').html(data);
                $("#delete_job").modal("show");
            }
        }
    });
}

function submit_delete_job(id) {
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/career/delete_job/' + id,
        cache: false,
        success: function (data) {
            data = JSON.parse(data);
            if (data.status) {
                location.reload();
            } else {
                $('.display_reminder_message').html("Can't Delete!");
                setTimeOut(3000);
            }
        }
    });
}

function changeStatus(id, status) {
    if(status == 0){
        status = 1;
    }else{
        status = 0;
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/newsletter/change_status/' + id + "/" + status,
        cache: false,
        success: function (data) {
            $('#status'+id).html(data);

        }
    });
}

function delContact(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/contact/delete/'+id,
        cache: false,
        success: function (data) {
           data = JSON.parse(data);
            if (data.status) {
                location.reload();
            } 
        }
    });
}

function delContactModal(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/contact/delete_confirmation/' + id,
        cache: false,
        success: function (data) {
            if (data) {
                $('.modal_container').html(data);
                $("#delete_contact").modal("show");
            }
        }
    });
}

function delNews(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/news/delete/'+id,
        cache: false,
        success: function (data) {
           data = JSON.parse(data);
            if (data.status) {
                location.reload();
            } 
        }
    });
}

function delNewsModal(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/news/delete_confirmation/' + id,
        cache: false,
        success: function (data) {
            if (data) {
                $('.modal_container').html(data);
                $("#delete_news").modal("show");
            }
        }
    });
}




