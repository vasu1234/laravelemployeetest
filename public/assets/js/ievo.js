$(document).ready(function () {
    $("#submit_admin_login").submit(function (e) {
        e.preventDefault();
        $('.error').remove()
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + '/login/login_admin',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    window.location = site_url + "dashboard";
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $("#member_form").submit(function (e) {
        e.preventDefault();
        $('.error').remove()
        var formData = new FormData($('#member_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "add/" + id;
        } else {
            url = site_url + "add";
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $("#submit_member_login").submit(function (e) {
        e.preventDefault();
        $('.error').remove()
        var formData = new FormData($('#submit_member_login')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + 'login/login_member',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    window.location = site_url + "dashboard";
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $("#member_profile").submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#member_profile')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + 'profile/update',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $("#update_password").click(function (e) {
        $('.error').remove();
        var formData = new FormData($('#update_password')[0]);
        $.ajax({
            type: 'POST',
            url: site_url + 'profile/password_modal',
            cache: false,
            success: function (data) {
                $('.modal_container').html(data);
                $("#modal_password").modal("show");
            }
        });
    });


    $("#credit_assessment_form").submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#credit_assessment_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "creditassessment/add/" + id;
        } else {
            var url = site_url + "creditassessment/add";
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    $("#member_assessment_form").submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#member_assessment_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "creditassessment/add/" + id;
        } else {
            var url = site_url + "creditassessment/add";
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });



    /******************************** Category jquery and Ajax function ****************************************/


    /*** ADD Parent Category ***/
    $('#parent-category-form').on('submit', function (event) {
        event.preventDefault();
        $('.error').remove();
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "category/add/" + id;
        } else {
            var url = site_url + "category/add";
        }
        $.ajax({
            url: url,
            type: "post",
            data: new FormData($('#parent-category-form')[0]),
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    /*** ADD Category ***/
    $('#category-form').on('submit', function (event) {
        event.preventDefault();
        $('.error').remove();
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "category/added_category/" + id;
        } else {
            var url = site_url + "category/added_category";
        }
        $.ajax({
            url: url,
            type: "post",
            data: new FormData($('#category-form')[0]),
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    /*** ADD Sub Category ***/
    $('#sub-category-form').on('submit', function (event) {
        event.preventDefault();
        $('.error').remove();
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "category/added_sub_category/" + id;
        } else {
            var url = site_url + "category/added_sub_category";
        }
        $.ajax({
            url: url,
            type: "post",
            data: new FormData($('#sub-category-form')[0]),
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    /*** Fetch category of parent category ***/

    $('#parent_category_id').change(function () {
        var id = $(this).val();
        $.ajax({
            url: site_url + "category/getCateogy/" + id,
            type: "post",
            success: function (result) {
                $('#category_id').html(result);
            }
        });
    });


    $('#add_attribute').click(function () {
        $.ajax({
            url: site_url + "category/addMoreAttribute/",
            type: "post",
            success: function (result) {
                $('.modal_container').html(result);
                $('#add_attr').modal('show');
            }
        });
    });

    /***************** Category attribute form ****************/
    $('#add_category_attribute').click(function () {
        $.ajax({
            url: site_url + "category/addMoreCategoryAttribute/",
            type: "post",
            success: function (result) {
                $('.modal_container').html(result);
                $('#add_cat_attr').modal('show');
            }
        });
    });

    /*****************Sub Category attribute form ****************/
    $('#add_sub_category_attribute').click(function () {
        $.ajax({
            url: site_url + "category/addMoreSubCategoryAttribute/",
            type: "post",
            success: function (result) {
                $('.modal_container').html(result);
                $('#add_sub_cat_attr').modal('show');
            }
        });
    });

    $('#add_banner').click(function () {
        $.ajax({
            url: site_url + "banner/addMoreBanner/",
            type: "post",
            success: function (result) {
                $('#modal_container').html(result);
                $('#add_banner_modal').modal('show');
            }
        });
    });

    $('#modal_container').on('submit', '#saveBanner', function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#saveBanner')[0]);
        var id = $('#banner_id').val();
        if (id != '') {
            var url = site_url + 'banner/add/' + id;
        } else {
            var url = site_url + 'banner/add';
        }

        $.ajax({
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class='error' style='color:red'>" + data.msg + "</span>");
                }
            }
        });
    });




    /********************************End Category jquery and Ajax function ****************************************/



    $("#seller_form").submit(function (e) {
        e.preventDefault();
        $('.error').remove()
        var formData = new FormData($('#seller_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "add/" + id;
        } else {
            url = site_url + "add";
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });


// seller login
    $("#submit_seller_login").submit(function (e) {
        e.preventDefault();
        $('.error').remove()
        var formData = new FormData($('#submit_seller_login')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + 'login/login_seller',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    var msg = `<div class="alert alert-success" id="result"><span>Login successfull.</span></div>`;
                    $('#err').html(msg);
                    window.location = site_url + "dashboard";
                } else {
                    var msg = `<div class="alert alert-danger" id="result"><span>${data.msg}</span></div>`
                    $('#err').html(msg);
                }
            }
        });
    });

    // seller profile

    $("#seller_profile").submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#seller_profile')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + 'profile/update',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });

    //update seller password
    $("#update_seller_password").click(function (e) {
        $('.error').remove();
        var formData = new FormData($('#update_seller_password')[0]);
        $.ajax({
            type: 'POST',
            url: site_url + 'profile/password_modal',
            cache: false,
            success: function (data) {
                $('.modal_container').html(data);
                $("#modal_password").modal("show");
            }
        });
    });

    //add brand
    $("#brand_form").submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#brand_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            url = site_url + "brand/add/" + id;
        } else {
            url = site_url + "brand/add";
        }
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class=\"error\"  style=\"color:#fc2d42\">" + data.msg + "</sapn>");
                }
            }
        });
    });


// seller product add jquery
    $('.rdo_two').click(function () {
        $('#search-category').show();
        $('#serach_list').show();
        $('#search-product').hide();
        $('#serarch_data').hide();
    });

    $('.rdo_one').click(function () {
        $('#search-category').hide();
        $('#search-product').show();
        $('#serarch_data').hide();
    });

    $('#next_btn').click(function () {
        $('.error').remove();
        $('.nav-item ').removeClass('active');
        $('#SPU-tab').parent().addClass('active');
        $('#SPU-tab').addClass('active');
        $('#sub_category').html($('#category_para').html());
    });

    $('#product_back').click(function () {
        $('.nav-item ').removeClass('active');
        $('#product-tab').parent().addClass('active');
        $('#product-tab').addClass('active');
    });

    $('#image_btn').click(function () {
        $('.error').remove();
        if ($('#name').val() == "") {
            $('#name').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }

        if ($('#warranty_type').val() == "") {
            $('#warranty_type').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        if ($('#warranty_time').val() == "") {
            $('#warranty_time').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        $('.nav-item ').removeClass('active');
        $('#image-tab').parent().addClass('active');
        $('#image-tab').addClass('active');
    });

    $('#finish_btn').click(function () {
        $('.error').remove();
        if ($('#SKUname').val() == "") {
            $('#SKUname').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }

        if ($('#price').val() == "") {
            $('#price').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        if ($('#package_weight').val() == "") {
            $('#package_weight').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        if ($('#package_length').val() == "") {
            $('#package_length').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }

        if ($('#package_width').val() == "") {
            $('#package_width').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        if ($('#package_height').val() == "") {
            $('#package_height').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        $('.nav-item ').removeClass('active');
        $('#finish-tab').parent().addClass('active');
        $('#finish-tab').addClass('active');
    });

    $('#spu_back').click(function () {
        $('.nav-item ').removeClass('active');
        $('#SPU-tab').parent().addClass('active');
        $('#SPU-tab').addClass('active');
    });

    $('#img_back').click(function () {
        $('.nav-item ').removeClass('active');
        $('#details-tab').parent().addClass('active');
        $('#details-tab').addClass('active');
    });

    $('#spu_edit').click(function () {
        $('.nav-item ').removeClass('active');
        $('#SPU-tab').parent().addClass('active');
        $('#SPU-tab').addClass('active');
    });

    $('#details_btn').click(function () {
        $('.error').remove();
        if ($('#sub_category').html() == "") {
            $('#sub_category').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Please go back and selete category first.</sapn>");
            return false;
        }
        if ($('#parent_id').val() == "") {
            $('#pr').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        if ($('#model').val() == "") {
            $('#model').after("<sapn class=\"error\"  style=\"color:#fc2d42\">Value is required and can't be empty</sapn>");
            return false;
        }
        $('.nav-item ').removeClass('active');
        $('#details-tab').parent().addClass('active');
        $('#details-tab').addClass('active');
    });


    // end seller add product jquery   

    // search product 
    $('#search_product').submit(function (e) {
        e.preventDefault();
        var formData = new FormData($('#search_product')[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + 'product/getProduct',
            cache: false,
            success: function (data) {
                $('#serach_list').hide();
                $('#serarch_data').show();
                $(".search_ul li").remove();
                data = JSON.parse(data);
                console.log(data);
                if (data.length > 0) {
                    $.each(data, function (key, value) {
                        $(".search_ul").append('<li ><a href="javascript:void(0)" onclick="get_cat(' + value['category_id'] + ',this,3)">' + value['parent_path'] + '</a></li>');
                    });
                } else {
                    $('#category_para').html('');
                    $('#next_btn').addClass('btn-default');
                    $('#next_btn').removeClass('btn-primary');
                    $('#next_btn').attr('disabled', true);
                    $(".search_ul").append('<li><a href="javascript:void(0)"><span class="tab">No data found</span></a></li>');
                }
            }
        });
    });

    // add warranty type
    $('#warranty_form').submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#warranty_form')[0]);
        var id = $('#id').val();
        if (id != "") {
            var url = site_url + "warranty/add/" + id;
        } else {
            var url = site_url + "warranty/add";
        }
        $.ajax({
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class='error' style='color:red'>" + data.msg + "<span>");
                }
            }
        });
    });

    $('#warranty_period_form').submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#warranty_period_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = site_url + 'warranty/add_period/' + id;
        } else {
            var url = site_url + 'warranty/add_period';
        }
        $.ajax({
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class='error' style='color:red'>" + data.msg + "</span>");
                }
            }
        });
    });

    $('#color_form').submit(function (e) {
        e.preventDefault();
        $('.error').remove();
        var formData = new FormData($('#color_form')[0]);
        var id = $('#id').val();
        if (id != '') {
            var url = site_url + 'color/add/' + id;
        } else {
            var url = site_url + 'color/add';
        }
        $.ajax({
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                } else {
                    $('#' + data.key).after("<sapn class='error' style='color:red'>" + data.msg + "</span>");
                }
            }
        });
    });

    //add product
    $('#add_product').submit(function (e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var id = $('#product_id').val();
        if (id != "") {
            url = site_url + "product/add/" + id;
        } else {
            url = site_url + "product/add";
        }
        var formData = new FormData($(this)[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: url,
            cache: false,
            success: function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                if (data.status) {
                    location.reload();
                }
            }
        });
    });


    $('#spu_name').change(function () {
        var spu = $.trim($(this).val());
        var name = convertToSlug(spu);
        $.ajax({
            type: 'POST',
            data: {name: name},
            url: site_url + "product/check_unique_product",
            success: function (result) {
                $('#slug').val(result);
            }
        });
    });

    $('.upload_image').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data');
        var formData = new FormData($('#upload_image_' + id)[0]);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: site_url + "product/save_product_image/" + id,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    $("#img_" + id).load(site_url + "product/manage_image #img_" + id);
                    $('#upload_image_' + id)[0].reset();
                }
            }
        });
    });
    $('#image_upload_data').on('click','.del_image',function () {
        var id = $(this).attr('data');
        var isThis = this;
        $.ajax({
            type: 'GET',
            url: site_url + "product/delete_product_image/" + id,
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    $(isThis).parent().remove();
                }
            }
        });
    });



});

function convertToSlug(Text)
{
    return Text
            .toLowerCase()
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '-')
            .replace(/[^\w-]+/g, '-')
            ;
}

function changeStatus(id) {
    var value = $('#check_' + id).val();
    if (value == 1) {
        var status = 0;
    } else {
        var status = 1;
    }
    $.ajax({
        type: 'POST',
        data: {status: status},
        url: site_url + "product/changeStatus/" + id,
        cache: false,
        dataType:"json",
        success: function (data) {
            if (data.status) {
                $('#check_' + id).val(data.value);
                toastr.success(data.msg);
            }else{
                toastr.danger(data.msg);
            }
        }
    });
}
//get category
function getCat(id, ele, level) {

    $.ajax({
        type: 'GET',
        data: {level: level},
        url: site_url + 'product/get_category/' + id,
        cache: false,
        success: function (data) {
            $(ele).parent().parent().parent().nextAll().remove();
            $(ele).parent().parent().parent().after(data);
            getCatAttribte(id, level);
            $('#parent_category_id').val(id);
        }
    });
}

function getSubCat(id, ele, level) {

    $.ajax({
        type: 'GET',
        data: {level: level},
        url: site_url + 'product/get_sub_category/' + id,
        cache: false,
        success: function (data) {
            $(ele).parent().parent().parent().nextAll().remove();
            $(ele).parent().parent().parent().after(data);
            hasChild(id);
            getCatAttribte(id, level);
            if(level == 2){
                $('#child_category_id').val(id);
            }
        }
    });
}

function getCatAttribte(id, level) {
    $.ajax({
        type: 'GET',
        data: {level: level},
        url: site_url + 'product/get_cat_attribute/' + id,
        cache: false,
        success: function (data) {
            if(level == 1){
                $('#attributes_data').html('<h4>Parent Attribute :</h4>'+data);
                $('#attributes_data_2').html('');
                $('#attributes_data_3').html('');
            }else if(level == 2){
                $('#attributes_data_2').html('<h4>Category Attribute :</h4>'+data);
                $('#attributes_data_3').html('');
            }else if(level == 3){
                if(data != "") {
                    $('#attributes_data_3').html('<h4>SubCategory Attribute :</h4>'+data);
                }
            }
        }
    });
}


function getPrarent(id, level) {
    $.ajax({
        type: 'GET',
        url: site_url + 'product/getParent/' + id,
        cache: false,
        success: function (data) {
            $('#category_para').html(data);
            $('#category_id').val(id);
        }
    });
}

function getPrarentCategory(id) {
    $.ajax({
        type: 'GET',
        url: site_url + 'product/getParent/' + id,
        cache: false,
        success: function (data) {
            $(".search_ul").append('<li ><a href="javascript:void(0)" onclick="get_cat(' + id + ',this)">' + data + '</a></li>');
        }
    });
}

function hasChild(id) {
    $.ajax({
        type: 'GET',
        url: site_url + 'product/haschild/' + id,
        cache: false,
        success: function (data) {
            if (data == true) {
                $('#next_btn').removeClass('btn-default');
                $('#next_btn').addClass('btn-primary');
                $('#next_btn').removeAttr('disabled');
            } else {
                $('#next_btn').addClass('btn-default');
                $('#next_btn').removeClass('btn-primary');
                $('#next_btn').attr('disabled', true);
            }
        }
    });
}

function get_cat(id, ele, level) {
    $('#category_para').html($(ele).html());
    $('#category_id').val(id);
    $('#level').val(level);
    $('#next_btn').removeClass('btn-default');
    $('#next_btn').addClass('btn-primary');
    $('#next_btn').removeAttr('disabled');
}

function addMoreRowInAttribute(count) {


    var row = `<div class = "input-icon attrubute_val<?php echo $count; ?>" >
            <input type = "text" name = "value[]" placeholder = "Attribute Value" class = "form-control "/>
            <a href = "javascript:void(0);" class = "btn mtop10 btn-danger" onclick = "remove(this);"> <i class = "fa fa-minus" aria - hidden = "true"> </i></a >
            <br>
            </div>`;
            $(".attrubute_val").append(row);

}

function remove(n) {
    $(n).parent().remove();
    return false;
}

function editAttribute(id) {
    $.ajax({
        url: site_url + "category/editMoreAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#add_attr').modal('show');
        }
    });
}

function deleteAtrribute(id) {
    $.ajax({
        url: site_url + "category/deleteAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#del_attr').modal('show');
        }
    });
}

function editCatAttribute(id) {
    $.ajax({
        url: site_url + "category/editMoreCategoryAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#add_cat_attr').modal('show');
        }
    });
}

function deleteCatAtrribute(id) {
    $.ajax({
        url: site_url + "category/deleteCategoryAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#del_cat_attr').modal('show');
        }
    });
}

function editSubCatAttribute(id) {
    $.ajax({
        url: site_url + "category/editMoreSubCategoryAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#add_sub_cat_attr').modal('show');
        }
    });
}

function deleteSubCatAtrribute(id) {
    $.ajax({
        url: site_url + "category/deleteSubCategoryAttribute/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#del_sub_cat_attr').modal('show');
        }
    });
}


function editBanner(id) {
    $.ajax({
        url: site_url + "banner/editMoreBanner/" + id,
        type: "post",
        success: function (result) {
            $('.modal_container').html(result);
            $('#add_banner_modal').modal('show');
        }
    });
}



