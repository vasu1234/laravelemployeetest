<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title> | Test Laravel </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
        type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css"
        id="style_color" />
    <link href="{{ asset('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/flag-icon-css-master/css/flag-icon.min.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/custom/css/my_style.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-duallistbox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/mystyle.css') }}">
    <!-- BEGIN JQUERY PLUGINS -->
    <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/prettify/r298/run_prettify.min.js"></script> -->
    <script src="{{ asset('assets/js/jquery.bootstrap-duallistbox.js') }}"></script>
    <!-- END JQUERY PLUGINS -->

    <!-- Latest Sortable -->
    <!-- <script src="{{ asset('assets/js/Sortable.js') }}"></script> -->

    <script type="text/javascript" src="{{ asset('assets/js/st/prettify/prettify.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/st/prettify/run_prettify.js') }}"></script>

    <!-- <script src="{{ asset('assets/js/st/app.js') }}"></script> -->
</head>
<!-- END HEAD -->
<!-- END HEAD -->
<style>
thead,
th {
    text-align: center;
}

.dt-head-center {
    text-align: center;
}
</style>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ asset('admin/dashboard') }}">
                        Test Laravel
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
                    data-target=".navbar-collapse">
                    <span></span>
                </a>
                <div class="col-sm-4">
                    <h4 class="date_time">
                        &nbsp;
                        <!--?php date_default_timezone_set("Asia/Singapore"); echo date("l, d-m-Y H:i:s"); ?-->
                    </h4>
                </div>
           
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle"
                                    src="{{ asset('assets/layouts/layout/img/avatar.png') }}" />
                                <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <!-- <li>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </li> -->
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-light " data-keep-expanded="false"
                        data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler"> <span></span> </div>
                        </li>
                        <!-- END SIDEBAR TOGGLER BUTTON -->
                        <li class="nav-item "> <a href="{{ route('price-master') }}" class="nav-link"> <i
                                    class="fa fa-money" aria-hdden="true"></i> <span class="title">Price
                                    Master</span>
                            </a> </li>
                        <li class="nav-item "> <a href="{{ route('book-stock') }}" class="nav-link">
                                <i class="fa fa-book" aria-hidden="true"></i> <span class="title">SOH
                                    Managment</span> </a> </li>
                        @if(session()->has('schedule_id'))
                        <li class="nav-item "> <a
                                href="{{ url('physical-inventory') }}/{{ session()->get('schedule_id') }}"
                                class="nav-link">
                                <i class="fa fa-sort" aria-hidden="true"></i> <span class="title">P
                                    I Managment</span> </a> </li>
                        @endif
                        <li class="nav-item "> <a href="{{ route('variance-analysis') }}" class="nav-link">
                                <i class="fa fa-link" aria-hidden="true"></i> <span class="title">
                                    Variance Analysis</span> </a> </li>
                        <li class="nav-item "> <a href="{{ route('report-analysis') }}" class="nav-link">
                                <i class="fa fa-book" aria-hidden="true"></i> <span class="title">
                                    Report Analysis</span> </a> </li>


                        <!-- 0./Dashboard-->
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            @yield('content')
            <!-- <div class="loader"></div> -->
            <div id="loading">
                <img id="loading-image" src="{{ asset('assets/images/ajax-loader.gif') }}" alt="Loading..." />
                <h4 style="margin-top: 351px;margin-left: 170px; font-weight: 600;">Please wait...</h4>
            </div>
        </div>
        <!-- BEGIN FOOTER -->
        <div class="modal_container"></div>
        <div class="page-footer">
            <div class="page-footer-inner"> 2021-<?= date('y') ?> &copy;</div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER -->
    </div>
    <!-- BEGIN QUICK NAV -->
    <div class="quick-nav-overlay"></div>
    <!-- START REMINDER POPUP -->
    <!-- START ALLOCATE ORDER POPUP -->
    <div id="allocate" class="modal fade" role="dialog" tabindex="-1" data-focus-on="input:first">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold text-center">Choose the Sequence</h4>
                </div>
                <div class="modal-body">
                    <h5 class="bold">Choose the Sequence(Select One)</h5>
                    <p>
                        <label class="mt-radio mt-radio-single mt-radio-outline">
                            <input type="radio" name="allocate_option" class="checkboxes" value="1" />
                            <span></span>
                            Option 1
                        </label><br />
                        <label class="mt-radio mt-radio-single mt-radio-outline">
                            <input type="radio" name="allocate_option" class="checkboxes" value="1" />
                            <span></span>
                            Option 2
                        </label><br />
                        <label class="mt-radio mt-radio-single mt-radio-outline">
                            <input type="radio" name="allocate_option" class="checkboxes" value="1" />
                            <span></span>
                            Option 3
                        </label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                    <button class="btn blue" data-toggle="modal" href="#allocate1" data-dismiss="modal">Next</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- END ALLOCATE ORDER POPUP -->
    <!-- END REMINDER POPUP -->
    <!-- END QUICK NAV -->

    <!-- BEGIN CORE PLUGINS -->
    <script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
        type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
        type="text/javascript">
    </script>

    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript">
    </script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
        type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
        type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
        type="text/javascript">
    </script>
    <script src="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript">
    </script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript">
    </script>

    <script>
    $(document).ready(function() {

        $('table.data-table').DataTable({
            "initComplete": function() {
                var api = this.api();

                api.$('td').click(function() {
                    if (!$(this).hasClass('disable_click')) {
                        api.search(this.innerHTML).draw();
                    }
                });
            },
            "bPaginate": false,
        });

    });

    $('.modal').on('hide.bs.modal', function() {
        $('input[type=file]').val("");
    });
    </script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
    <script>
    $(document).ready(function() {
        $(".select2").select2({
            placeholder: "Select an option",
            allowClear: false
        });
        //  $('table.table').dataTable({"bPaginate": true , "info" : true});
        $("#speakers").select2({
            placeholder: "Select an option",
            multiple: true,
            allowClear: false,
            minimumResultsForSearch: 5
        });
        $("#sponsor").select2({
            placeholder: "Select an option",
            multiple: true,
            allowClear: false,
            minimumResultsForSearch: 5
        });
    });
    </script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript">
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/moment/moment.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('assets/layouts/layout/scripts/moment/moment-timezone-with-data.min.js') }}"
        type="text/javascript"></script>

    <script src="{{ asset('assets/layouts/layout/scripts/moment/moment-timezone-utils.js') }}" type="text/javascript">
    </script>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript">
    </script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/user/js/custom-form-elements.js') }}"></script>
    <!--<script src="{{ asset('assets/js/jquery.min.js" type="text/jscript"></script>-->

    <script>
    var timezone = 'Asia/kolkata';
    var update_top_date = function() {
        date = moment(new Date()).tz(timezone);

        $('.date_time').html(date.format('dddd, DD MMM YYYY, h:mm:ss a'));
    };



    update_top_date();
    setInterval(update_top_date, 1000);

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });

    function isURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    }
    </script>
    <script>
    $(window).load(function() {
        setTimeout(() => {
            $('#loading').hide();
        }, 500);
    });
    </script>
    <script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    </script>

    <!-- END THEME LAYOUT SCRIPTS -->
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>


<!-- END THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>