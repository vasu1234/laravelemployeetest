@extends('../layouts.default')

@section('content')

<!-- / INCLUDE HEADER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <div class="col-sm-10">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Department</span>
                    </li>
                </ul>
            </div>

        </div>

        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <!--h1 class="page-title"> Department Supports <small>Ticket System</small> </h1-->
        <div class="row">
            <div class="col-md-12 pad-15">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings"></i>
                            <span class="caption-subject bold uppercase"> Department Management
                            </span>
                        </div>
                        <div class="caption pull-right">
                            <i class="icon-left"></i>
                            <a href="javascript:void(0);" class="caption-subject bold white" onclick="goBack()"> <i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Back </a>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel">Add Department</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/action_page.php" id="addDepartment">
                                        <fieldset>
                                            <legend>Department</legend>
                                            <div class="form-group col-sm-6">
                                                <label for="email"> Name :<span class="required">*</span> <input
                                                        type="text" name="name" required id="name"
                                                        class="form-control" placeholder=" Enter Department name" required>
                                            </div>
                                           
                                           
                                        </fieldset>
                                    
                                      
                                    </form>
                                </div>
                                <div class="modal-footer" style="text-align:center">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="addBtn">Add Department</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#exampleModal">
                                        Add Department
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                    </div>
                                </div>
                            </div>

                        </div>


                        <table class="table table-bordered" id='item-list'>
                            <thead class="cf">
                                <tr>
                                    <th>
                                        S.No.
                                    </th>
                                    <th> Name </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->




<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<div id="delete-model" class="modal fade" role="dialog" tabindex="-1" data-focus-on="input:first"
    data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title bold text-center" id='confirmation'>Confirmation!</h4>
            </div>
            <div class="modal-body" id="view-data">
                <h5>Do your want to change delete status of Department <b id='Department-name'></b> ?</h5>
            </div>
            <div class="modal-footer" id='model-footer'>
                <button type="button" id="delete-Department" delete-Department-id="" class="btn btn-outline red">Change</button>
                <button class="btn blue" data-toggle="modal" href="#allocate1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="modal fade" id="editDepartmentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">Edit Department</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="editDepartment">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updateBtn">Update Department</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewDepartmentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h4 class="modal-title" id="viewModalLabel"> Department</h4> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="viewDepartment">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
// $('body').on('click', '.DepartmentType', function() {
//     let DepartmentType = $(this).val();
//     if (DepartmentType == 1) {
//         $('#addBtn').html('Add Department');
//     } else {
//         $('#addBtn').html('Add Contractor');
//     }

// });

$('body').on('click', '#addBtn', function(e) {
    e.preventDefault();
    /*Ajax Request Header setup*/
    $('.alert').remove();
    let ithis = this;
    $(this).prop('disabled', true);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let formData = new FormData($('#addDepartment')[0]);
    $.ajax({
        type: "POST",
        url: "{{ url('add-department')}}",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
            console.log(data);
            let html = '';
                html = `<div class="alert alert-success">
  <strong>Success!</strong> Department added successfully..
</div>`;
                setTimeout(function() {
                    location.reload();
                }, 2000);
            
            $('#exampleModalLabel').after(html);
            $(ithis).prop('disabled', false);
        }
    });
});

$('body').on('click', '#updateBtn', function(e) {
    e.preventDefault();
    /*Ajax Request Header setup*/
    $('.alert').remove();
    let ithis = this;
    $(this).prop('disabled', true);
    let DepartmentId = $("#DepartmentId").val();
    let formData = new FormData($('#editDepartmentForm')[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "{{ url('department-update')}}" + "/" + DepartmentId,
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
            console.log(data);
            let html = '';
                html = `<div class="alert alert-success">
  <strong>Success!</strong> Department updated successfully..
</div>`;
                setTimeout(function() {
                    location.reload();
                }, 2000);
           
            $('#editModalLabel').after(html);
            $(ithis).prop('disabled', false);
        }
    });
});
</script>
<script>
$(document).ready(function() {
    var dataTable = $('#item-list').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '{{ url("department-list") }}', // json datasource
            type: "get", // method  , by default get
            error: function() { // error handling
                $(".department-grid-error").html("");
                $("#department-grid").append(
                    '<tbody class="department-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>'
                );
                $("#department-grid_processing").css("display", "none");

            }
        },
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                mRender: function(data, type, row) {
                    
                        return "<a  href='javascript:void(0)' class='edit'  edit-id='" +
                            row['id'] +
                            "' ><i class='fa fa-edit'></i>  </a>" + "  " +
                            "&nbsp;&nbsp;&nbsp;<a  href='javascript:void(0)' delete-Department='" +
                            row['name'] + "' class='delete'  delete-id='" +
                            row['id'] +
                            "'   ><i class='fa fa-trash'></i> </a>"
                },
                'orderable': false
            },
        ],
        drawCallback: function() {
            $('.active-Department').parent().parent().addClass("td-color");
        }

    });

    dataTable.columns().every(function() {
        var table = this;
        $('input', this.header()).on('keyup change', function() {
            if (table.search() !== this.value) {
                table.serach(this.value).draw();
            }
        })
    });

    $('.search-input-text').on('keyup click', function() { // for text boxes

        var i = $(this).attr('data-column'); // getting column index
        var v = $(this).val(); // getting search input value
        dataTable.columns(i).search(v).draw();

    });


    $('body').on('click', '.view-record', function() {
        let viewId = $(this).attr('view-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("department-view") }}' + "/"+viewId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                // $('#heading').html("Event's Detail");
                $('#viewDepartment').html(data);
                $('#viewDepartmentModel').modal('show');
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });

    $('body').on('click', '.edit', function() {
        let editId = $(this).attr('edit-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("department-edit") }}' + "/" + editId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                $('#editDepartment').html(data);
                $('#editDepartmentModel').modal('show');
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });
    $('body').on('click', '.delete', function() {
        let deleteId = $(this).attr('delete-id');
        $('#delete-Department').attr('delete-department-id', deleteId);
        $('#Department-name').html($(this).attr('delete-Department'));
        $('#delete-model').modal('show');
    });

    $('body').on('click', '#delete-Department', function() {
        let DepartmentId = $(this).attr('delete-Department-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("Department-delete") }}' + "/" + DepartmentId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                let success = `<div class="alert alert-success">
                            <strong>Success!</strong>  <a href="#" class="alert-link">Department delete status changed.</a>.
                            </div>`
                $('#confirmation').after(success);
                setTimeout(() => {
                    window.location.reload();
                }, 500);
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });


    $('.active-Department').parent().parent().addClass('td-color');

});
</script>
@endsection