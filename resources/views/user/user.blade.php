@extends('../layouts.default')

@section('content')

<!-- / INCLUDE HEADER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <div class="col-sm-10">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>User</span>
                    </li>
                </ul>
            </div>
            
        </div>

        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <!--h1 class="page-title"> Customer Supports <small>Ticket System</small> </h1-->
        <div class="row">
            <div class="col-md-12 pad-15">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings"></i>
                            <span class="caption-subject bold uppercase"> User Managment
                            </span>
                        </div>
                        <div class="caption pull-right">
                            <i class="icon-left"></i>
                            <a href="javascript:void(0);" class="caption-subject bold white" onclick="goBack()"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back </a>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel">Add User</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/action_page.php" id="addUser">
                                     
                                        <fieldset>
                                            <legend>User Details</legend>
                                            <div class="form-group col-sm-6">
                                                <label for="User">Username :<span class="required">*</span><input type="text" name="usernamechar" required id="usernamechar" class="form-control" placeholder="Username">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="User">First Name :<span class="required">*</span><input type="text" name="first_name" required id="first_name" class="form-control" placeholder=" First Name">
                                            </div>


                                            <div class="form-group col-sm-6">
                                                <label for="email"> Last Name: <input type="text" name="last_name" required id="last_name" class="form-control" placeholder=" Last Name">
                                            </div>
                                            <div class=" form-group col-sm-6">
                                                <label for="email"> Email :<span class="required">*</span> <input type="emai" name="email" required id="email" class="form-control" placeholder=" email">
                                            </div>
                                       



                                        </fieldset>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="addBtn">Add User</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Add User
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                    </div>
                                </div>
                            </div>

                        </div>


                        <table class="table table-bordered" id='item-list'>
                            <thead class="cf">
                                <tr>
                                    <th>
                                        S.No.
                                    </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Email </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->




<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<div id="delete-model" class="modal fade" role="dialog" tabindex="-1" data-focus-on="input:first" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title bold text-center" id='confirmation'>Confirmation!</h4>
            </div>
            <div class="modal-body" id="view-data">
                <h5>Do your want to change delete status of user <b id='user-name'></b> ?</h5>
            </div>
            <div class="modal-footer" id='model-footer'>
                <button type="button" id="delete-user" delete-user-id="" class="btn btn-outline red">Change</button>
                <button class="btn blue" data-toggle="modal" href="#allocate1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="modal fade" id="editUserModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">Edit User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="editUser">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updateBtn">Update User</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('click', '.userType', function() {
        let userType = $(this).val();
        if (userType == 1) {
            $('#addBtn').html('Add Employee');
        } else {
            $('#addBtn').html('Add Contractor');
        }

    });

    $('body').on('click', '#addBtn', function(e) {
        e.preventDefault();
        /*Ajax Request Header setup*/
        $('.alert').remove();
        let ithis = this;
        $(this).prop('disabled', true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData($('#addUser')[0]);
        $.ajax({
            type: "POST",
            url: "{{ url('add-user')}}",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                console.log(data);
                let html = '';
                if (data.status) {
                    html = `<div class="alert alert-success">
  <strong>Success!</strong> User added successfully..
</div>`;
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    html = `<div class="alert alert-danger">
  <strong>Fail!</strong>${data.error}
</div>`;
                }
                $('#exampleModalLabel').after(html);
                $(ithis).prop('disabled', false);
            }
        });
    });

    $('body').on('click', '#updateBtn', function(e) {
        e.preventDefault();
        /*Ajax Request Header setup*/
        $('.alert').remove();
        let ithis = this;
        $(this).prop('disabled', true);
        let userId = $("#userId").val();
        let formData = new FormData($('#editUserForm')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ url('user-update')}}" + "/" + userId,
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                console.log(data);
                let html = '';
                if (data.status) {
                    html = `<div class="alert alert-success">
  <strong>Success!</strong> User updated successfully..
</div>`;
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    html = `<div class="alert alert-danger">
  <strong>Fail!</strong>${data.error}
</div>`;
                }
                $('#editModalLabel').after(html);
                $(ithis).prop('disabled', false);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        var dataTable = $('#item-list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: '{{ url("user-list") }}', // json datasource
                type: "get", // method  , by default get
                error: function() { // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append(
                        '<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>'
                    );
                    $("#employee-grid_processing").css("display", "none");

                }
            },
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'last_name',
                    name: 'last_name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    mRender: function(data, type, row) {
                      
                            return "<a  href='javascript:void(0)' class='edit'  edit-id='" +
                                row['id'] +
                                "' ><i class='fa fa-edit'></i>  </a>" + "  " +
                                "&nbsp;&nbsp;&nbsp;<a  href='javascript:void(0)' delete-user='" +
                                row['name'] + "' class='delete'  delete-id='" +
                                row['id'] +
                                "'   ><i class='fa fa-trash'></i> </a>"
                      
                    },
                    'orderable': false
                },
            ],
            drawCallback: function() {
                $('.active-user').parent().parent().addClass("td-color");
            }

        });

        dataTable.columns().every(function() {
            var table = this;
            $('input', this.header()).on('keyup change', function() {
                if (table.search() !== this.value) {
                    table.serach(this.value).draw();
                }
            })
        });

        $('.search-input-text').on('keyup click', function() { // for text boxes

            var i = $(this).attr('data-column'); // getting column index
            var v = $(this).val(); // getting search input value
            dataTable.columns(i).search(v).draw();

        });


        $('body').on('click', '.view', function() {
            let viewId = $(this).attr('view-id');
            $.ajax({
                type: 'GET',
                url: '' + '/admin/event/show/' + viewId,
                cache: false,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(data) {
                    $('#heading').html("Event's Detail");
                    $('#view-data').html(data);
                    $('#model-footer').hide();
                    $('#event-model').modal('show');
                },
                complete: function() {
                    $('#loading').hide();
                }
            });
        });

        $('body').on('click', '.edit', function() {
            let editId = $(this).attr('edit-id');
            $.ajax({
                type: 'GET',
                url: '{{ url("user-edit") }}' + "/" + editId,
                cache: false,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(data) {
                    $('#editUser').html(data);
                    $('#editUserModel').modal('show');
                },
                complete: function() {
                    $('#loading').hide();
                }
            });
        });
        $('body').on('click', '.delete', function() {
            let deleteId = $(this).attr('delete-id');
            $('#delete-user').attr('delete-user-id', deleteId);
            $('#user-name').html($(this).attr('delete-user'));
            $('#delete-model').modal('show');
        });

        $('body').on('click', '#delete-user', function() {
            let userId = $(this).attr('delete-user-id');
            $.ajax({
                type: 'GET',
                url: '{{ url("user-delete") }}' + "/" + userId,
                cache: false,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(data) {
                    let success = `<div class="alert alert-success">
                            <strong>Success!</strong>  <a href="#" class="alert-link">User delete status changed.</a>.
                            </div>`
                    $('#confirmation').after(success);
                    setTimeout(() => {
                        window.location.reload();
                    }, 500);
                },
                complete: function() {
                    $('#loading').hide();
                }
            });
        });


        $('.active-user').parent().parent().addClass('td-color');

    });
</script>
@endsection