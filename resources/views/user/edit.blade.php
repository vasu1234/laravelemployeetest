<form id="editUserForm" type="post">

    <fieldset>
        <legend>User Details</legend>
        <div class=" form-group col-sm-6">
            <label for="email"> Username :<span class="required">*</span></label>
            <input type="emai" name="usernamechar" required id="usernamechar" value="{{ $user->usernamechar }}" class="form-control"
                placeholder=" email">
            <input type="hidden" id="userId" value="{{ $user->id }}">
        </div>

        <div class="form-group col-sm-6">
            <label for="User">First Name :<span class="required">*</span></label>
            <input type="text" name="name" required id="name" value="{{ $user->name }}" class="form-control"
                placeholder=" user name">
        </div>

        <div class="form-group col-sm-6">
            <label for="User">Last Name :<span class="required">*</span></label>
            <input type="text" name="last_name" required id="last_name" value="{{ $user->last_name }}" class="form-control"
                placeholder=" user last name">
        </div>

        <div class="form-group col-sm-6">
            <label for="email"> Email :</label>
            <input type="text" name="email" required id="email" value="{{ $user->email }}" class="form-control"
                placeholder="email">
        </div>



    </fieldset>
</form orm>