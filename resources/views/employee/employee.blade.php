@extends('../layouts.default')

@section('content')

<!-- / INCLUDE HEADER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <div class="col-sm-10">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Employee</span>
                    </li>
                </ul>
            </div>

        </div>

        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <!--h1 class="page-title"> Employee Supports <small>Ticket System</small> </h1-->
        <div class="row">
            <div class="col-md-12 pad-15">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings"></i>
                            <span class="caption-subject bold uppercase"> Employee Management
                            </span>
                        </div>
                        <div class="caption pull-right">
                            <i class="icon-left"></i>
                            <a href="javascript:void(0);" class="caption-subject bold white" onclick="goBack()"> <i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Back </a>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel">Add Employee</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/action_page.php" id="addEmployee">
                                        <fieldset>
                                            <legend>Basic Detail</legend>
                                            <div class="form-group col-sm-4">
                                                <label for="email">First Name :<span class="required">*</span> <input
                                                        type="text" name="first_name" required id="first_name"
                                                        class="form-control" placeholder=" Enter First name" required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Middle Name : <input
                                                        type="text" name="middle_name" required id="middle_name"
                                                        class="form-control" placeholder=" Enter middle name" >
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Last Name :<span class="required">*</span> <input
                                                        type="text" name="last_name" required id="last_name"
                                                        class="form-control" placeholder=" Enter Last name" required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Date Of Birth:<span class="required">*</span> <input
                                                        type="date" name="birthdate" required id="birthdate"
                                                        class="form-control"  required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Date Of Hired :<span class="required">*</span> <input
                                                        type="date" name="date_hired" required id="date_hired"
                                                        class="form-control"  required>
                                            </div>
                                           
                                        </fieldset>
                                        <fieldset>
                                            <legend>Address Detail</legend>
                                            <div class=" form-group col-sm-12">
                                                <label for="name"> Address :<span class="required">*</span> <textarea
                                                        type="text" name="address" required id="address"
                                                        class="form-control" placeholder="Enter address" required></textarea>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> ZIP Code : <input
                                                        type="text" name="zip" required id="zip"
                                                        class="form-control" placeholder="Enter zip code" required>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> Department : <select class="form-control" name="department_id">
                                                    <option value="">Select Department</option>
                                                    @foreach($department as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> City : <select class="form-control" name="city_id">
                                                    <option value="">Select City</option>
                                                    @foreach($city as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> State : <select class="form-control" name="state_id">
                                                    <option value="">Select State</option>
                                                    @foreach($state as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> Country : <select class="form-control" name="country_id">
                                                    <option value="">Select Country</option>
                                                    @foreach($country as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                           
                                        </fieldset>
                                      
                                    </form>
                                </div>
                                <div class="modal-footer" style="text-align:center">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="addBtn">Add Employee</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#exampleModal">
                                        Add Employee
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                    </div>
                                </div>
                            </div>

                        </div>


                        <table class="table table-bordered" id='item-list'>
                            <thead class="cf">
                                <tr>
                                    <th>
                                        S.No.
                                    </th>
                                    <th> Name </th>
                                    <th> Address </th>
                                    <th> ZIP </th>
                                    <th> Hired Date </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->




<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<div id="delete-model" class="modal fade" role="dialog" tabindex="-1" data-focus-on="input:first"
    data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title bold text-center" id='confirmation'>Confirmation!</h4>
            </div>
            <div class="modal-body" id="view-data">
                <h5>Do your want to change delete status of Employee <b id='Employee-name'></b> ?</h5>
            </div>
            <div class="modal-footer" id='model-footer'>
                <button type="button" id="delete-Employee" delete-Employee-id="" class="btn btn-outline red">Change</button>
                <button class="btn blue" data-toggle="modal" href="#allocate1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="modal fade" id="editEmployeeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">Edit Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="editEmployee">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updateBtn">Update Employee</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewEmployeeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h4 class="modal-title" id="viewModalLabel"> Employee</h4> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="viewEmployee">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
// $('body').on('click', '.EmployeeType', function() {
//     let EmployeeType = $(this).val();
//     if (EmployeeType == 1) {
//         $('#addBtn').html('Add Employee');
//     } else {
//         $('#addBtn').html('Add Contractor');
//     }

// });

$('body').on('click', '#addBtn', function(e) {
    e.preventDefault();
    /*Ajax Request Header setup*/
    $('.alert').remove();
    let ithis = this;
    $(this).prop('disabled', true);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let formData = new FormData($('#addEmployee')[0]);
    $.ajax({
        type: "POST",
        url: "{{ url('add-employee')}}",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
                html = `<div class="alert alert-success">
  <strong>Success!</strong> Employee added successfully..
</div>`;
                setTimeout(function() {
                    location.reload();
                }, 2000);
           
            $('#exampleModalLabel').after(html);
            $(ithis).prop('disabled', false);
        }
    });
});

$('body').on('click', '#updateBtn', function(e) {
    e.preventDefault();
    /*Ajax Request Header setup*/
    $('.alert').remove();
    let ithis = this;
    $(this).prop('disabled', true);
    let EmployeeId = $("#EmployeeId").val();
    let formData = new FormData($('#editEmployeeForm')[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "{{ url('employee-update')}}" + "/" + EmployeeId,
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
            console.log(data);
            let html = '';
                html = `<div class="alert alert-success">
  <strong>Success!</strong> Employee updated successfully..
</div>`;
                setTimeout(function() {
                    location.reload();
                }, 2000);
            
            $('#editModalLabel').after(html);
            $(ithis).prop('disabled', false);
        }
    });
});
</script>
<script>
$(document).ready(function() {
    var dataTable = $('#item-list').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '{{ url("employee-list") }}', // json datasource
            type: "get", // method  , by default get
            error: function() { // error handling
                $(".employee-grid-error").html("");
                $("#employee-grid").append(
                    '<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>'
                );
                $("#employee-grid_processing").css("display", "none");

            }
        },
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'address',
                name: 'address'
            },
            {
                data: 'zip',
                name: 'zip'
            }, 
            {
                data: 'date_hired',
                name: 'date_hired'
            },
            {
                mRender: function(data, type, row) {
                    
                        return "<a  href='javascript:void(0)' class='edit'  edit-id='" +
                            row['id'] +
                            "' ><i class='fa fa-edit'></i>  </a>" + "  " +
                            "&nbsp;&nbsp;&nbsp;<a  href='javascript:void(0)' delete-Employee='" +
                            row['name'] + "' class='delete'  delete-id='" +
                            row['id'] +
                            "'   ><i class='fa fa-trash'></i> </a>"
                },
                'orderable': false
            },
        ],
        drawCallback: function() {
            $('.active-Employee').parent().parent().addClass("td-color");
        }

    });

    dataTable.columns().every(function() {
        var table = this;
        $('input', this.header()).on('keyup change', function() {
            if (table.search() !== this.value) {
                table.serach(this.value).draw();
            }
        })
    });

    $('.search-input-text').on('keyup click', function() { // for text boxes

        var i = $(this).attr('data-column'); // getting column index
        var v = $(this).val(); // getting search input value
        dataTable.columns(i).search(v).draw();

    });


    $('body').on('click', '.view-record', function() {
        let viewId = $(this).attr('view-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("employee-view") }}' + "/"+viewId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                // $('#heading').html("Event's Detail");
                $('#viewEmployee').html(data);
                $('#viewEmployeeModel').modal('show');
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });

    $('body').on('click', '.edit', function() {
        let editId = $(this).attr('edit-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("employee-edit") }}' + "/" + editId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                $('#editEmployee').html(data);
                $('#editEmployeeModel').modal('show');
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });
    $('body').on('click', '.delete', function() {
        let deleteId = $(this).attr('delete-id');
        $('#delete-Employee').attr('delete-employee-id', deleteId);
        $('#Employee-name').html($(this).attr('delete-Employee'));
        $('#delete-model').modal('show');
    });

    $('body').on('click', '#delete-Employee', function() {
        let EmployeeId = $(this).attr('delete-Employee-id');
        $.ajax({
            type: 'GET',
            url: '{{ url("Employee-delete") }}' + "/" + EmployeeId,
            cache: false,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(data) {
                let success = `<div class="alert alert-success">
                            <strong>Success!</strong>  <a href="#" class="alert-link">Employee delete status changed.</a>.
                            </div>`
                $('#confirmation').after(success);
                setTimeout(() => {
                    window.location.reload();
                }, 500);
            },
            complete: function() {
                $('#loading').hide();
            }
        });
    });


    $('.active-Employee').parent().parent().addClass('td-color');

});
</script>
@endsection