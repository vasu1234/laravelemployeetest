<form action="/action_page.php" id="editcustomerForm">
<fieldset>
<fieldset>
                                            <legend>Basic Detail</legend>
                                            <div class="form-group col-sm-4">
                                                <label for="email">First Name :<span class="required">*</span> <input
                                                        type="text" name="first_name" required id="first_name"
                                                        class="form-control" placeholder=" Enter First name" required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Middle Name : <input
                                                        type="text" name="middle_name" required id="middle_name"
                                                        class="form-control" placeholder=" Enter middle name" >
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Last Name :<span class="required">*</span> <input
                                                        type="text" name="last_name" required id="last_name"
                                                        class="form-control" placeholder=" Enter Last name" required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Date Of Birth:<span class="required">*</span> <input
                                                        type="text" name="birthdate" required id="birthdate"
                                                        class="form-control"  required>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="email">Date Of Hired :<span class="required">*</span> <input
                                                        type="text" name="date_hired" required id="date_hired"
                                                        class="form-control"  required>
                                            </div>
                                           
                                        </fieldset>
                                        <fieldset>
                                            <legend>Address Detail</legend>
                                            <div class=" form-group col-sm-12">
                                                <label for="name"> Address :<span class="required">*</span> <textarea
                                                        type="text" name="address" required id="address"
                                                        class="form-control" placeholder="Enter address" required></textarea>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> ZIP Code : <input
                                                        type="text" name="zip" required id="zip"
                                                        class="form-control" placeholder="Enter zip code" required>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> Department : <select class="form-control" name="department_id">
                                                    <option value="">Select Department</option>
                                                    @foreach($department as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> City : <select class="form-control" name="city_id">
                                                    <option value="">Select City</option>
                                                    @foreach($city as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> State : <select class="form-control" name="state_id">
                                                    <option value="">Select State</option>
                                                    @foreach($state as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" form-group col-sm-4">
                                                <label for="name"> Country : <select class="form-control" name="country_id">
                                                    <option value="">Select Country</option>
                                                    @foreach($country as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                           
                                        </fieldset>

    
</form>