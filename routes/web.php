<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Employees routes
Route::get('/employee', 'EmployeeController@index');
Route::get('/employee-list', 'EmployeeController@EmployeeList');
Route::get('/employee-edit/{id}', 'EmployeeController@EmployeeEdit');
Route::post('/add-employee', 'CommonController@createEmployee');
Route::post('/employee-update/{id}', 'EmployeeController@updateEmployee');
Route::get('/employee-delete/{id}', 'EmployeeController@EmployeeDelete');

// Department routes
Route::get('/department', 'DepartmentController@index');
Route::get('/department-list', 'DepartmentController@DepartmentList');
Route::get('/department-edit/{id}', 'DepartmentController@DepartmentEdit');
Route::post('/add-department', 'CommonController@createDepartment');
Route::post('/department-update/{id}', 'DepartmentController@updateDepartment');
Route::get('/department-delete/{id}', 'DepartmentController@DepartmentDelete');

// user routes
Route::get('/user', 'UserController@index');
Route::get('/user-list', 'UserController@userList');
Route::get('/user-edit/{id}', 'UserController@userEdit');
Route::post('/add-user', 'UserController@add');
Route::get('/user-view/{id}', 'UserController@userView');
Route::post('/user-update/{id}', 'UserController@updateUser');
Route::get('/user-delete/{id}', 'UserController@userDelete');